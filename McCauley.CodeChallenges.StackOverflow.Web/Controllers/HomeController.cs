﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using McCauley.CodeChallenges.StackOverflow.Web.Clients;
using McCauley.CodeChallenges.StackOverflow.Web.Infrastructure;
using McCauley.CodeChallenges.StackOverflow.Web.ViewModels;
using McCauley.CodeChallenges.StackOverflow.Web.ViewModels.QuestionDetails;
using McCauley.CodeChallenges.StackOverflow.Web.ViewModels.RecentQuestions;

namespace McCauley.CodeChallenges.StackOverflow.Web.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IQuestionsClient _questionsClient;
        private readonly IAnswersClient _answersClient;
        private readonly IFiltersClient _filtersClient;

        public HomeController(
            ILogger<HomeController> logger,
            IQuestionsClient questionsClient,
            IAnswersClient answersClient,
            IFiltersClient filtersClient)
        {
            _logger = logger;
            _questionsClient = questionsClient;
            _answersClient = answersClient;
            _filtersClient = filtersClient;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Index()
        {
            var resultSet = await _questionsClient.GetQuestions();
            var questions = resultSet.Items;
            var viewModel = new PageViewModel
            {
                Questions = questions.Select(q => new QuestionViewModel
                {
                    QuestionId = q.Question_Id,
                    Title = q.Title,
                    Link = q.Link
                }).ToList()
            };
            return View(viewModel);
        }

        [HttpGet]
        [Route("question/{id:int}")]
        public async Task<IActionResult> Question(int id)
        {
            var filterResultSet = await _filtersClient.Create(new[] {"question.body", "answer.body"}, false, "default");
            var filter = filterResultSet.Items.FirstOrDefault();

            var questionIds = new[] {id};
            var questions = await _questionsClient.GetQuestions(questionIds, filter?.Key);

            // Should only expect 1 question returned
            if (questions?.Items == null || questions.Items.Count != 1)
            {
                return NotFound();
            }

            var question = questions.Items.First();
            var answers = await _answersClient.GetAnswersForQuestions(questionIds, filter?.Key);

            var viewModel = new QuestionDetailViewModel
            {
                QuestionId = question.Question_Id,
                Title = question.Title,
                Body = question.Body,
                Answers = answers.Items.Where(a => a.Question_Id == question.Question_Id).Select(a => new QuestionAnswerViewModel
                {
                    AnswerId = a.Answer_Id,
                    Body = a.Body
                }).ToList().Shuffle()
            };

            return View(viewModel);
        }

        [HttpGet]
        [Route("guess-answer/{questionId:int}/{chosenAnswerId:int}")]
        public async Task<IActionResult> GuessAnswer(int questionId, int chosenAnswerId)
        {
            var filterResultSet = await _filtersClient.Create(new[] { "answer.body" }, false, "default");
            var filter = filterResultSet.Items.FirstOrDefault();

            var answerIds = new[] { chosenAnswerId };
            var answers = await _answersClient.GetAnswers(answerIds, filter?.Key);

            // Should only expect 1 answer returned
            if (answers?.Items == null || answers.Items.Count != 1)
            {
                return NotFound();
            }

            var answer = answers.Items.First();
            var response = new GuessAnswerResponseViewModel
            {
                IsCorrect = answer.Is_Accepted,
                Message = answer.Is_Accepted ? "Correct! This is the accepted answer!" : "This is not the accepted answer."
            };
            return Json(response);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
