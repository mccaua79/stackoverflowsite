﻿using System.Net;
using System.Threading.Tasks;
using McCauley.CodeChallenges.StackOverflow.Web.Infrastructure;
using McCauley.CodeChallenges.StackOverflow.Web.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;
using RestSharp.Serialization;

namespace McCauley.CodeChallenges.StackOverflow.Web.Clients
{
    public interface IFiltersClient
    {
        Task<ApiResultSet<Filter>> Create(string[] includedFields, bool unSafeFlag, string baseFilter);
    }

    public class FiltersClient : ApiClientBase, IFiltersClient
    {
        public FiltersClient(
            IRestClient client,
            IRestSerializer serializer, 
            ILogger<AnswersClient> logger,
            IOptions<AppConfig> appSettings,
            IMemoryCache cache) : base(client, serializer, logger, appSettings, cache)
        {
        }

        public async Task<ApiResultSet<Filter>> Create(string[] includedFields, bool unSafeFlag = false, string baseFilter = null)
        {
            var response = await Get<ApiResultSet<Filter>>($"2.2/filters/create?include={string.Join(";", includedFields)}{(unSafeFlag ? "&unsafe=true" : string.Empty)}{(!string.IsNullOrWhiteSpace(baseFilter) ? $"&base={baseFilter}" : string.Empty)}");
            return response?.StatusCode == HttpStatusCode.OK ? response.Data : null;
        }
    }
}
