﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using McCauley.CodeChallenges.StackOverflow.Web.Infrastructure;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;
using RestSharp.Serialization;

namespace McCauley.CodeChallenges.StackOverflow.Web.Clients
{
	public abstract class ApiClientBase
	{
		public static readonly string DateTimeFormat = "yyyy/MM/dd/HH/mm";

		public readonly IMemoryCache Cache;
		private readonly IRestSerializer _serializer;

		protected readonly ILogger<ApiClientBase> Logger;

		protected ApiClientBase(
			IRestClient client,
			IRestSerializer serializer,
			ILogger<ApiClientBase> logger,
			IOptions<AppConfig> appSettings,
			IMemoryCache cache)
		{
			Client = client;
			Client.BaseUrl = new Uri(appSettings.Value.ApiBaseUrl);

			DataFormat = DataFormat.Json;
			_serializer = serializer;
			Client.UseSerializer(() => _serializer);
			Logger = logger;
			Cache = cache;
		}

		public DataFormat DataFormat { get; set; }

		public IRestClient Client { get; }

		protected async Task<IRestResponse> Get(
			string url,
			Dictionary<string, object> parameters,
			ParameterType parameterType = ParameterType.UrlSegment)
		{
			return await Invoke(url, parameters, null, Method.GET, parameterType);
		}

		protected async Task<IRestResponse<T>> Get<T>(string url) where T : new()
		{
			return await Invoke<T>(url, null, null, Method.GET, ParameterType.UrlSegment);
		}

		// = ParameterType.UrlSegment
		protected async Task<IRestResponse<T>> Get<T>(
			string url,
			Dictionary<string, object> parameters,
			ParameterType parameterType = ParameterType.UrlSegment) where T : new()
		{
			return await Invoke<T>(url, parameters, null, Method.GET, parameterType);
		}

		protected async Task<IRestResponse<T>> Get<T>(
			string url,
			Dictionary<string, object> parameters,
			object body,
			ParameterType parameterType = ParameterType.UrlSegment) where T : new()
		{
			return await Invoke<T>(url, parameters, body, Method.GET, parameterType);
		}

		protected async Task<IRestResponse<T>> Put<T>(string url, object body) where T : new()
		{
			return await Invoke<T>(url, null, body, Method.PUT, ParameterType.UrlSegment);
		}

		protected async Task<IRestResponse<T>> Put<T>(
			string url,
			Dictionary<string, object> parameters,
			ParameterType parameterType = ParameterType.UrlSegment) where T : new()
		{
			return await Invoke<T>(url, parameters, null, Method.PUT, parameterType);
		}

		protected async Task<IRestResponse<T>> Put<T>(
			string url,
			Dictionary<string, object> parameters,
			object body,
			ParameterType parameterType = ParameterType.UrlSegment) where T : new()
		{
			return await Invoke<T>(url, parameters, body, Method.PUT, parameterType);
		}

		protected async Task<IRestResponse<T>> PostFile<T>(string url, string name, byte[] file) where T : new()
		{
			var req = GetRequest(url, Method.POST);
			req.AddFile("file", file, name, MediaTypeNames.Application.Octet);
			return await Invoke<T>(req);
		}

		protected async Task<IRestResponse> Post(string url, object body)
		{
			return await Invoke(url, null, body, Method.POST, ParameterType.UrlSegment);
		}

		protected async Task<IRestResponse<T>> Post<T>(string url, object body) where T : new()
		{
			return await Invoke<T>(url, null, body, Method.POST, ParameterType.UrlSegment);
		}

		protected async Task<IRestResponse<T>> Post<T>(
			string url,
			Dictionary<string, object> parameters,
			ParameterType parameterType = ParameterType.UrlSegment) where T : new()
		{
			return await Invoke<T>(url, parameters, null, Method.POST, parameterType);
		}

		protected async Task<IRestResponse<T>> Post<T>(
			string url,
			Dictionary<string, object> parameters,
			object body,
			ParameterType parameterType = ParameterType.UrlSegment) where T : new()
		{
			return await Invoke<T>(url, parameters, body, Method.POST, parameterType);
		}

		protected async Task<IRestResponse<T>> Delete<T>(
			string url,
			Dictionary<string, object> parameters,
			ParameterType parameterType = ParameterType.UrlSegment) where T : new()
		{
			return await Invoke<T>(url, parameters, null, Method.DELETE, parameterType);
		}

		protected async Task<IRestResponse> Invoke(
			string url,
			Dictionary<string, object> parameters,
			object body,
			Method method,
			ParameterType parameterType)
		{
			var req = BuildRequest(url, parameters, body, method, parameterType);

			return await Invoke(req);
		}

		protected async Task<IRestResponse<T>> Invoke<T>(
			string url,
			Dictionary<string, object> parameters,
			object body,
			Method method,
			ParameterType parameterType) where T : new()
		{
			var req = BuildRequest(url, parameters, body, method, parameterType);

			return await Invoke<T>(req);
		}

		private static void AddBody(object body, Method method, IRestRequest req)
		{
			if (body == null)
			{
				return;
			}

			switch (method)
			{
				case Method.GET:
					req.AddObject(body);
					break;
				default:
					req.AddJsonBody(body);
					break;
			}
		}

		private void AddParameters(
			string url,
			Dictionary<string, object> parameters,
			Method method,
			ParameterType parameterType,
			IRestRequest req)
		{
			var parameterLog = GetLog(url, method);
			if (parameters != null)
			{
				foreach (var p in parameters)
				{
					// TODO - log params, but not the sensitive ones.
					req.AddParameter(p.Key, p.Value != null ? p.Value.ToString() : String.Empty, parameterType);
				}
			}

			Logger.LogInformation(parameterLog.ToString());
		}

		private RestRequest BuildRequest(
			string url,
			Dictionary<string, object> parameters,
			object body,
			Method method,
			ParameterType parameterType)
		{
			var req = GetRequest(url, method);

			AddParameters(url, parameters, method, parameterType, req);

			AddBody(body, method, req);
			return req;
		}

		private StringBuilder GetLog(string url, Method method)
		{
			var parameterLog = new StringBuilder();

			parameterLog.AppendLine($"Base Url : {Client.BaseUrl};");
			parameterLog.AppendLine(
                $"ApiClient - Method: {method}, URL: {url}, DataFormat: {DataFormat}");
			//parameterLog.AppendLine( "Parameters: " );
			return parameterLog;
		}

		protected async Task<IRestResponse> Invoke(IRestRequest request)
		{
			request.AddHeader("Accept", "application/json");

			var response = await Client.ExecuteAsync(request);

			if (response.ResponseStatus == ResponseStatus.Completed)
			{
				return response;
			}

			if (response.ErrorException != null)
			{
				throw response.ErrorException;
			}

			throw new Exception($"Error {response.ResponseStatus}: {response.ErrorMessage ?? "unknown error"}");

			//TODO: ERROR handling see below
		}

		protected async Task<IRestResponse<T>> Invoke<T>(IRestRequest request) where T : new()
		{
			request.AddHeader("Accept", "application/json");

			var response = await Client.ExecuteAsync<T>(request);
			if (response.ResponseStatus != ResponseStatus.Completed)
			{
				if (response.ErrorException != null)
				{
					Logger.LogError($"Error {response.ResponseStatus}: {response.ErrorMessage ?? "unknown error"}");
				}

				response.StatusCode = HttpStatusCode.InternalServerError;
			}

			if ((int)response.StatusCode >= 400 && !string.IsNullOrWhiteSpace(response.Content))
			{
				Logger.LogError($"Error from API Call: Url: {response.ResponseUri}  Response Content: {response.Content}");
			}

			return response;
		}

		protected async Task<bool> InvokeBool(IRestRequest request)
		{
			request.AddHeader("Accept", "application/json");
			var response = await Execute(request);

			if (bool.TryParse(response.Content, out var ret))
			{
				return ret;
			}

			// ToDo:  Unhandled exception
			throw new Exception("Unhandled exception!");
		}

		private static void HandleException(IRestResponse response)
		{
			if (response.StatusCode == HttpStatusCode.OK && response.ResponseStatus != ResponseStatus.Error &&
				response.StatusCode != HttpStatusCode.InternalServerError)
				return;

			var message = string.Empty;

			if (!string.IsNullOrWhiteSpace(response.ErrorMessage))
			{
				message = response.ErrorMessage;
			}

			if (!string.IsNullOrWhiteSpace(response.StatusDescription))
			{
				message = message + "\r\n" + response.StatusDescription;
			}

			// TODO: Custom exception class				
			throw new Exception(
                $"Error {response.ResponseStatus}: {(!string.IsNullOrWhiteSpace(message) ? message : "unknown error")}");
		}


		private async Task<IRestResponse> Execute(IRestRequest restRequest)
		{
			var response = await Client.ExecuteAsync(restRequest);
			HandleException(response);
			return response;
		}

		/// <summary>
		///     Builds a basic auth header to be sent to the API.
		///     This is used in conjunction with the BasicAuthModule custom IIS module.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="tokenId"></param>
		protected static void AddAuthenticationHeader(IRestRequest request, string tokenId)
		{
			if (string.IsNullOrEmpty(tokenId))
			{
				return;
			}

			var bytes = Encoding.UTF8.GetBytes(tokenId);
			var token = Convert.ToBase64String(bytes);
			request.AddHeader("Authorization", String.Format("Basic {0}", token));
		}

		protected RestRequest GetRequest(string url, Method method)
		{
			var req = new RestRequest(url, method) { RequestFormat = DataFormat, JsonSerializer = _serializer };
			return req;
		}
	}
}