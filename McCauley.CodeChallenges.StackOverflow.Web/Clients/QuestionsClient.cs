﻿using System.Net;
using System.Threading.Tasks;
using McCauley.CodeChallenges.StackOverflow.Web.Infrastructure;
using McCauley.CodeChallenges.StackOverflow.Web.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;
using RestSharp.Serialization;

namespace McCauley.CodeChallenges.StackOverflow.Web.Clients
{
    public interface IQuestionsClient
    {
        Task<ApiResultSet<Question>> GetQuestions();
        Task<ApiResultSet<Question>> GetQuestions(int[] ids, string filter);
    }

    public class QuestionsClient : ApiClientBase, IQuestionsClient
    {
        public QuestionsClient(
            IRestClient client,
            IRestSerializer serializer, 
            ILogger<QuestionsClient> logger,
            IOptions<AppConfig> appSettings,
            IMemoryCache cache) : base(client, serializer, logger, appSettings, cache)
        {
        }

        public async Task<ApiResultSet<Question>> GetQuestions()
        {
            var response = await Get<ApiResultSet<Question>>("2.2/search/advanced/?order=desc&sort=activity&accepted=True&answers=2&site=StackOverflow");
            return response?.StatusCode == HttpStatusCode.OK ? response.Data : null;
        }

        public async Task<ApiResultSet<Question>> GetQuestions(int[] ids, string filter = null)
        {
            var response = await Get<ApiResultSet<Question>>($"2.2/questions/{string.Join(";", ids)}?site=stackoverflow{(!string.IsNullOrWhiteSpace(filter) ? $"&filter={filter}" : string.Empty)}");
            return response?.StatusCode == HttpStatusCode.OK ? response.Data : null;
        }
    }
}
