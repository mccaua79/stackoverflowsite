﻿using System.Net;
using System.Threading.Tasks;
using McCauley.CodeChallenges.StackOverflow.Web.Infrastructure;
using McCauley.CodeChallenges.StackOverflow.Web.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;
using RestSharp.Serialization;

namespace McCauley.CodeChallenges.StackOverflow.Web.Clients
{
    public interface IAnswersClient
    {
        Task<ApiResultSet<Answer>> GetAnswersForQuestions(int[] questionIds, string filter);
        Task<ApiResultSet<Answer>> GetAnswers(int[] answerIds, string filter);
    }

    public class AnswersClient : ApiClientBase, IAnswersClient
    {
        public AnswersClient(
            IRestClient client,
            IRestSerializer serializer, 
            ILogger<AnswersClient> logger,
            IOptions<AppConfig> appSettings,
            IMemoryCache cache) : base(client, serializer, logger, appSettings, cache)
        {
        }

        public async Task<ApiResultSet<Answer>> GetAnswersForQuestions(int[] questionIds, string filter = null)
        {
            var response = await Get<ApiResultSet<Answer>>($"2.2/questions/{string.Join(";", questionIds)}/answers?site=stackoverflow{(!string.IsNullOrWhiteSpace(filter) ? $"&filter={filter}" : string.Empty)}");
            return response?.StatusCode == HttpStatusCode.OK ? response.Data : null;
        }

        public async Task<ApiResultSet<Answer>> GetAnswers(int[] answerIds, string filter)
        {
            var response = await Get<ApiResultSet<Answer>>($"2.2/answers/{string.Join(";", answerIds)}?site=stackoverflow{(!string.IsNullOrWhiteSpace(filter) ? $"&filter={filter}" : string.Empty)}");
            return response?.StatusCode == HttpStatusCode.OK ? response.Data : null;
        }
    }
}
