﻿using System.Collections.Generic;

namespace McCauley.CodeChallenges.StackOverflow.Web.Models
{
    public class ApiResultSet<T> where T : new()
    {
        public List<T> Items { get; set; }
    }
}
