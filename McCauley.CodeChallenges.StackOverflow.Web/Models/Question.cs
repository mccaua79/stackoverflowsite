﻿namespace McCauley.CodeChallenges.StackOverflow.Web.Models
{
    public class Question
    {
        public bool Is_Answered { get; set; }
        public int Answer_Count { get; set; }
        public int Score { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public int Question_Id { get; set; }
        public string Body { get; set; }
    }
}
