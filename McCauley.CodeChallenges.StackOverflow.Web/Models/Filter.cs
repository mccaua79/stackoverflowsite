﻿using Newtonsoft.Json;

namespace McCauley.CodeChallenges.StackOverflow.Web.Models
{
    public class Filter
    {
        [JsonProperty("filter")]
        public string Key { get; set; }
        public string[] IncludedFields { get; set; }
        public string FilterType { get; set; }
    }
}
