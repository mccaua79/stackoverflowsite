﻿using Newtonsoft.Json;

namespace McCauley.CodeChallenges.StackOverflow.Web.Models
{
    public class Answer
    {
        [JsonProperty("question_id")] // TODO: These aren't mapping at all, is it a bad serializer? Should I use a custom Newtonsoft serializer?
        public int Question_Id;

        [JsonProperty("answer_id")]
        public int Answer_Id;
        public int Score;
        public string Body { get; set; }
        public bool Is_Accepted { get; set; }
    }
}
