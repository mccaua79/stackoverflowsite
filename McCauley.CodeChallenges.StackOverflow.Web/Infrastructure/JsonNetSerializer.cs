﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serialization;

namespace McCauley.CodeChallenges.StackOverflow.Web.Infrastructure
{
    // ReSharper disable once PossibleInterfaceMemberAmbiguity
    public interface IJsonNetSerializer : IRestSerializer { }

    public class JsonNetSerializer : IJsonNetSerializer
    {
        private readonly JsonSerializerSettings _settings;

        public JsonNetSerializer()
        {
            ContentType = "application/json";
            _settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            };
            DataFormat = DataFormat.Json;
            SupportedContentTypes = new[] { "application/json", "text/json", "text/x-json", "text/javascript", "*+json" };
        }

        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, _settings);
        }

        public T Deserialize<T>(IRestResponse response)
        {
            return JsonConvert.DeserializeObject<T>(response.Content, _settings);
        }

        public string RootElement { get; set; }
        public string Namespace { get; set; }
        public string DateFormat { get; set; }
        public string ContentType { get; set; }
        public string Serialize(Parameter parameter)
        {
            throw new System.NotImplementedException();
        }

        public string[] SupportedContentTypes { get; set; }

        public DataFormat DataFormat { get; set; }
    }
}
