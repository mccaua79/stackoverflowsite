﻿namespace McCauley.CodeChallenges.StackOverflow.Web.Infrastructure
{
    public class AppConfig
    {
        public string ApiBaseUrl { get; set; }
    }
}
