﻿$(document).ready(function() {
    bindControls();
});

function bindControls() {
    $("button[data-choose-btn]").on("click",
        function () {
            var url = $(this).data("chooseBtn");
            var answerId = $(this).data("answerId");
            guessAnswer(url, answerId);
        });
};

function guessAnswer(url, answerId) {
    clearAnswerText();
    $.ajax({
        url: url,
        type: "GET",
        success: function (response) {
            if (response != null) {
                displayChooseMessage(answerId, response.isCorrect, response.message);
            } else {
                displayChooseMessage(answerId, false, "Unable to complete request.");
            }
        },
        error: function(response) {
            displayChooseMessage(answerId, false, "Server error. Could not complete request.");
        }
    });
};

function displayChooseMessage(answerId, isCorrect, message) {
    var answerObj = $("span[data-answer-text=" + answerId + "]");
    var alertClass = isCorrect ? "text-success" : "text-danger";
    answerObj.text(message).removeClass().addClass(alertClass).show();
};

function clearAnswerText() {
    $("span[data-answer-text]").text("").hide();
};