using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using McCauley.CodeChallenges.StackOverflow.Web.Clients;
using McCauley.CodeChallenges.StackOverflow.Web.Infrastructure;
using RestSharp;
using RestSharp.Serialization;

namespace McCauley.CodeChallenges.StackOverflow.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddRouting(opts => opts.LowercaseUrls = true);
            services.Configure<AppConfig>(Configuration.GetSection("AppConfig"));
            services.AddTransient<IRestClient, RestClient>();
            //services.AddTransient<IRestSerializer, JsonSerializer>();
            services.AddTransient<IRestSerializer, JsonNetSerializer>();
            services.AddTransient<IQuestionsClient, QuestionsClient>();
            services.AddTransient<IAnswersClient, AnswersClient>();
            services.AddTransient<IFiltersClient, FiltersClient>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
