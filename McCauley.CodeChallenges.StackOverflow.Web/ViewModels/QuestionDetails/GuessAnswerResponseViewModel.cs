﻿namespace McCauley.CodeChallenges.StackOverflow.Web.ViewModels.QuestionDetails
{
    public class GuessAnswerResponseViewModel
    {
        public bool IsCorrect { get; set; }
        public string Message { get; set; }
    }
}
