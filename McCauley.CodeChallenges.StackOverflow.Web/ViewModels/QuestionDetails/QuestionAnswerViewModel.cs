﻿namespace McCauley.CodeChallenges.StackOverflow.Web.ViewModels.QuestionDetails
{
    public class QuestionAnswerViewModel
    {
        public int AnswerId { get; set; }
        public string Body { get; set; }
    }
}
