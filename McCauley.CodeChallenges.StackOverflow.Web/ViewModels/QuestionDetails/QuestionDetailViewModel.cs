﻿using System.Collections.Generic;

namespace McCauley.CodeChallenges.StackOverflow.Web.ViewModels.QuestionDetails
{
    public class QuestionDetailViewModel
    {
        public int QuestionId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public IList<QuestionAnswerViewModel> Answers { get; set; }
    }
}
