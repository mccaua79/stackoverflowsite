﻿namespace McCauley.CodeChallenges.StackOverflow.Web.ViewModels.RecentQuestions
{
    public class QuestionViewModel
    {
        public int QuestionId { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
    }
}
