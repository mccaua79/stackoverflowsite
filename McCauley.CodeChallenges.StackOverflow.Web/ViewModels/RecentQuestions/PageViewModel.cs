﻿using System.Collections.Generic;

namespace McCauley.CodeChallenges.StackOverflow.Web.ViewModels.RecentQuestions
{
    public class PageViewModel
    {
        public List<QuestionViewModel> Questions { get; set; }
    }
}
